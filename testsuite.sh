#!/bin/bash

function killtest() {
	while kill $(pgrep -f "$1") 2>/dev/null; do sleep 1; wait; done
}

function runtest() {
	make $1 iso
	echo -n > /tmp/KOS.serial
	make $1 all
	timeout 300 make $1 $2 &
	sleep 3
	case $2 in
		qemu)  pidstring="qemu-system-x86_64.*KOS";;
		vbox)  pidstring=".*/lib/.*VirtualBox.*KOS";;
		bochs) pidstring="bochs.*kos";;
		*)     echo $2; exit 1;;
	esac
	trap "killtest "$pidstring"; exit 1" EXIT
	while pgrep -f "$1" >/dev/null && ! fgrep -q "OUT OF MEMORY" /tmp/KOS.serial; do
		sleep 3;
	done
	killtest "$pidstring"
	if fgrep -q "FP mismatch" /tmp/KOS.serial; then exit 1; fi
}

if [ $# -ge 1 ]; then
	for target in ${*}; do
		echo RUNNING: "" "$target"
		runtest "" "$target"
	done
	exit 0
fi

for compile in gcc clang gccdebug clangdebug
do
	make clean
	case $compile in
		gcc)        flags="";;
		gccdebug)   flags="OPTIM=0";;
		clang)      flags="CC=clang";;
		clangdebug) flags="CC=clang OPTIM=0";;
		*)          echo $compile; exit 1;
	esac
	make $flags all
	for target in qemu vbox bochs
	do
		echo RUNNING: "$compile" "$flags" "$target"
		runtest "$flags" "$target"
		cp /tmp/KOS.serial /tmp/KOS.serial.$target.$compile
	done
done
trap - EXIT
echo "TESTSUITE FINISHED - SUCCESS"
exit 0
