/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _StackContext_h_
#define _StackContext_h_ 1

#include "generic/IntrusiveContainers.h" 
#include "runtime/Runtime.h"
#include "runtime/Stack.h"

#include <map>

static const size_t  TopPriority = 0;
static const size_t  DefPriority = 1;
static const size_t  LowPriority = 2;
static const size_t  NumPriority = 3;

class BlockingLock;
class EventScope;
class SuspendHelper;
class ResumeInfo;
class BaseProcessor;
class Cluster;

#if TESTING_ENABLE_DEBUGGING
static const size_t StackLinkCount = 3;
#else
static const size_t StackLinkCount = 2;
#endif

template <size_t NUM> class StackList :
public IntrusiveList<StackContext,NUM,StackLinkCount,DoubleLink<StackContext,StackLinkCount>> {};

template <size_t NUM> class StackQueue :
public IntrusiveQueue<StackContext,NUM,StackLinkCount,DoubleLink<StackContext,StackLinkCount>> {};

template <size_t NUM> class StackMPSC :
#if TESTING_NEMESIS_READYQUEUE
public IntrusiveQueueNemesis<StackContext,NUM,StackLinkCount,DoubleLink<StackContext,StackLinkCount>> {};
#else
public IntrusiveQueueStub<StackContext,NUM,StackLinkCount,DoubleLink<StackContext,StackLinkCount>> {};
#endif

static const size_t ReadyQueueLink = 0;
typedef StackList<ReadyQueueLink> BlockedStackList;

#if TESTING_ENABLE_DEBUGGING
typedef StackList<1> GlobalStackList; // cf. GLOBAL_STACK_LINK in libfibre/libfibre.so-gdb.py
#endif

class StackContext : public DoubleLink<StackContext,StackLinkCount> {
  vaddr          stackPointer; // holds stack pointer while stack inactive
  BaseProcessor* processor;    // next resumption on this processor
  size_t         priority;     // scheduling priority
  bool           affinity;     // affinity prohibits re-staging

  enum SuspendState { Running, Prepared, Suspended };
  SuspendState volatile suspendState;
  ResumeInfo*  volatile resumeInfo;  // race: unblock vs. timeout

  StackContext(const StackContext&) = delete;
  const StackContext& operator=(const StackContext&) = delete;

  // central stack switching routine
  enum SwitchCode { Idle = 'I', Yield = 'Y', Migrate = 'M', Suspend = 'S', Terminate = 'T' };
  template<SwitchCode> inline void switchStack(StackContext& nextStack);

  // these routines are called immediately after the stack switch
  static void postIdle     (StackContext* prevStack);
  static void postYield    (StackContext* prevStack);
  static void postMigrate  (StackContext* prevStack);
  static void postSuspend  (StackContext* prevStack);
  static void postTerminate(StackContext* prevStack);

  void suspend();
  void resumeInternal();
  void changeProcessor(BaseProcessor&);

protected:
  // constructor/destructors can only be called by derived classes
  StackContext(BaseProcessor& proc, bool aff = false); // main constructor
  StackContext(Cluster&, bool bg = false);             // uses delegation
  ~StackContext() {
    GENASSERTN(suspendState == Running, FmtHex(this), suspendState);
    GENASSERT1(resumeInfo == nullptr, FmtHex(this));
  }

  void initStackPointer(vaddr sp) {
    stackPointer = align_down(sp, stackAlignment);
  }

public:
  // direct switch to new stack
  void direct(ptr_t func, _friend<SystemProcessor>) __noreturn {
    stackDirect(stackPointer, func, nullptr, nullptr, nullptr);
  }

  // set up new stack and resume for concurrent execution
  void setup(ptr_t func, ptr_t p1 = nullptr, ptr_t p2 = nullptr, ptr_t p3 = nullptr) {
    stackPointer = stackInit(stackPointer, func, p1, p2, p3);
  }

  // set up new stack and resume for concurrent execution
  void start(ptr_t func, ptr_t p1 = nullptr, ptr_t p2 = nullptr, ptr_t p3 = nullptr) {
    setup(func, p1, p2, p3);
    resumeInternal();
  }

  // context switching - static -> apply to CurrStack()
  static void idleYieldTo(StackContext& nextStack, _friend<BaseProcessor>);
  static bool yield();
  static bool yield2();
  static void preempt();
  static void terminate() __noreturn;

  // context switching - non-static -> restricted to SuspendHelper
  template<size_t SpinStart = 1, size_t SpinEnd = 0>
  void suspend(_friend<SuspendHelper>) {
    size_t spin = SpinStart;
    while (spin <= SpinEnd) {
      for (size_t i = 0; i < spin; i += 1) Pause();
      if (suspendState == Running) return;
      spin += spin;
    }
    suspend();
  }

  // Running -> Prepared; Prepared -> Suspended is attempted in postSuspend()
  void prepareSuspend(_friend<SuspendHelper>) {
    GENASSERTN(suspendState == Running, FmtHex(this), suspendState);
    __atomic_store_n( &suspendState, Prepared, __ATOMIC_RELAXED );
  }

  // Prepared/Suspended -> Running; resume stack later, if necessary
  bool checkResume() {
    SuspendState prevState = __atomic_exchange_n( &suspendState, Running, __ATOMIC_RELAXED );
    GENASSERTN(prevState != Running, FmtHex(this), prevState);
    return prevState == Suspended;
  }

  // resume stack now
  void resume() {
    if (checkResume()) resumeInternal();
  }

  // set ResumeInfo with resumption information
  void setupResumeRace(ResumeInfo& ri, _friend<SuspendHelper>) {
    GENASSERTN(suspendState == Prepared, FmtHex(this), suspendState);
    __atomic_store_n( &resumeInfo, &ri, __ATOMIC_RELAXED );
  }

  // race between different possible resumers -> winner cancels the others
  ResumeInfo* raceResume() {
    return __atomic_exchange_n( &resumeInfo, nullptr, __ATOMIC_RELAXED );
  }

  // set resume processor during scheduling
  void changeProcessor(BaseProcessor& rp, _friend<BaseProcessor>) { changeProcessor(rp); }
  BaseProcessor& getProcessor() { return *processor; }
  // priority
  StackContext* setPriority(size_t p) { priority = p; return this; }
  size_t getPriority() const          { return priority; }

  // hard affinity - no staging
  bool getAffinity()       { return affinity; }
  void setAffinity(bool a) { affinity = a; }

  // migration
  void rebalance();
  static void migrateSelf(Cluster&);
  static void migrateSelf(BaseProcessor&);
  static BaseProcessor& migrateSelf(Cluster&, _friend<EventScope>);
  static void migrateSelf(BaseProcessor&, _friend<EventScope>);
#if TESTING_ALWAYS_MIGRATE
  void forceMigrateNow();
#endif
};

#endif /* _StackContext_h_ */
