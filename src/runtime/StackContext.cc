/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "runtime/Cluster.h"
#include "runtime/StackContext.h"

StackContext::StackContext(BaseProcessor& proc, bool aff)
: stackPointer(0), processor(&proc), priority(DefPriority), affinity(aff),
  suspendState(Running), resumeInfo(nullptr) {
  processor->addStack(_friend<StackContext>());
}

StackContext::StackContext(Cluster& cluster, bool bg)
: StackContext(cluster.placement(_friend<StackContext>(), bg), bg) {}

template<StackContext::SwitchCode Code>
inline void StackContext::switchStack(StackContext& nextStack) {
  // various checks
  static_assert(Code == Idle || Code == Yield || Code == Migrate || Code == Suspend || Code == Terminate, "Illegal SwitchCode");
  CHECK_PREEMPTION(0);
  GENASSERTN(this == CurrStack() && this != &nextStack, FmtHex(this), ' ', FmtHex(CurrStack()), ' ', FmtHex(&nextStack));

  // context switch
  Runtime::debugS("Stack switch <", char(Code), "> on ", FmtHex(&CurrProcessor()),": ", FmtHex(this), " (to ", FmtHex(processor), ") -> ", FmtHex(&nextStack));
  Runtime::preStackSwitch(*this, nextStack);
  switch (Code) {
    case Idle:      stackSwitch(this, postIdle,      &stackPointer, nextStack.stackPointer); break;
    case Yield:     stackSwitch(this, postYield,     &stackPointer, nextStack.stackPointer); break;
    case Migrate:   stackSwitch(this, postMigrate,   &stackPointer, nextStack.stackPointer); break;
    case Suspend:   stackSwitch(this, postSuspend,   &stackPointer, nextStack.stackPointer); break;
    case Terminate: stackSwitch(this, postTerminate, &stackPointer, nextStack.stackPointer); break;
  }
  stackPointer = 0;                // mark stack in use for gdb
  Runtime::postStackSwitch(*this); // RT-specific functionality
}

// idle stack -> do nothing
void StackContext::postIdle(StackContext*) {
  CHECK_PREEMPTION(0);
}

// yield -> resume right away
void StackContext::postYield(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  prevStack->processor->enqueueDirect(*prevStack, _friend<StackContext>());
}

// yield -> resume right away
void StackContext::postMigrate(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  prevStack->resumeInternal();
}

// if resumption already triggered -> resume right away
void StackContext::postSuspend(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  SuspendState prevState = Prepared;
  bool suspended = __atomic_compare_exchange_n( &prevStack->suspendState, &prevState, Suspended, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED );
  if (!suspended) {
    GENASSERTN(prevState == Running, FmtHex(prevStack), prevState);
    prevStack->resumeInternal();
  }
}

// destroy stack
void StackContext::postTerminate(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  prevStack->processor->removeStack(_friend<StackContext>());
  Runtime::stackDestroy(*prevStack);
}

// a new thread/stack starts in stubInit() and then jumps to this routine
extern "C" void invokeStack(funcvoid3_t func, ptr_t arg1, ptr_t arg2, ptr_t arg3) {
  CHECK_PREEMPTION(0);
  Runtime::EnablePreemption();
  func(arg1, arg2, arg3);
  Runtime::DisablePreemption();
  StackContext::terminate();
}

void StackContext::resumeInternal() {
  processor->enqueueResume(*this, _friend<StackContext>());
}

void StackContext::idleYieldTo(StackContext& nextStack, _friend<BaseProcessor>) {
  CHECK_PREEMPTION(1);          // expect preemption still enabled
  Runtime::DisablePreemption();
  CurrStack()->switchStack<Idle>(nextStack);
  Runtime::EnablePreemption();
}

bool StackContext::yield() {
  CHECK_PREEMPTION(1);          // expect preemption still enabled
  Runtime::DisablePreemption();
  StackContext* nextStack = CurrProcessor().scheduleYield(_friend<StackContext>());
  if (nextStack) CurrStack()->switchStack<Yield>(*nextStack);
  Runtime::EnablePreemption();
  return nextStack;
}

bool StackContext::yield2() {
  CHECK_PREEMPTION(1);          // expect preemption still enabled
  Runtime::DisablePreemption();
  StackContext* nextStack = CurrProcessor().scheduleYield2(_friend<StackContext>());
  if (nextStack) CurrStack()->switchStack<Yield>(*nextStack);
  Runtime::EnablePreemption();
  return nextStack;
}

void StackContext::preempt() {
  StackContext* nextStack = CurrProcessor().scheduleYield(_friend<StackContext>());
  if (nextStack) CurrStack()->switchStack<Yield>(*nextStack);
}

void StackContext::terminate() {
  CurrStack()->switchStack<Terminate>(CurrProcessor().scheduleFull(_friend<StackContext>()));
  unreachable();
}

void StackContext::suspend() {
  switchStack<Suspend>(CurrProcessor().scheduleFull(_friend<StackContext>()));
}

void StackContext::changeProcessor(BaseProcessor& p) {
  processor->removeStack(_friend<StackContext>());
  processor = &p;
  processor->addStack(_friend<StackContext>());
}

void StackContext::rebalance() {
  affinity = false;
  changeProcessor(CurrCluster().placement(_friend<StackContext>(), true));
}

// migrate to cluster; adjust stackCounts, clear affinity
void StackContext::migrateSelf(Cluster& cluster) {
  migrateSelf(cluster.placement(_friend<StackContext>(), true));
}

// migrate to proessor; adjust stackCounts, clear affinity
void StackContext::migrateSelf(BaseProcessor& proc) {
  StackContext* sc = CurrStack();
  sc->affinity = false;
  sc->changeProcessor(proc);
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>(CurrProcessor().scheduleFull(_friend<StackContext>()));
  Runtime::EnablePreemption();
}

// migrate to cluster (for disk I/O), don't change stackCount or affinity
BaseProcessor& StackContext::migrateSelf(Cluster& cluster, _friend<EventScope>) {
  StackContext* sc = CurrStack();
  BaseProcessor* proc = sc->processor;
  sc->processor = &cluster.placement(_friend<StackContext>(), true);
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>(CurrProcessor().scheduleFull(_friend<StackContext>()));
  Runtime::EnablePreemption();
  return *proc;
}

// migrate back to previous processor (after disk I/O), don't change stackCount or affinity
void StackContext::migrateSelf(BaseProcessor& proc, _friend<EventScope>) {
  StackContext* sc = CurrStack();
  sc->processor = &proc;
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>(CurrProcessor().scheduleFull(_friend<StackContext>()));
  Runtime::EnablePreemption();
}

#if TESTING_ALWAYS_MIGRATE
void StackContext::forceMigrateNow() {
  StackContext* sc = CurrStack();
  if (sc->affinity) return;
  sc->changeProcessor(CurrCluster().placement(_friend<StackContext>(), true));
  sc->switchStack<Migrate>(CurrProcessor().scheduleFull(_friend<StackContext>()));
}
#endif
