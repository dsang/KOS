/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BaseProcessor_h_
#define _BaseProcessor_h_ 1

#include "runtime/StackContext.h"

class BasePoller;
class Cluster;
class Scheduler;

class StackResumeQueue {
#if TESTING_POLLER_DIRECT
  friend class ReadyQueue; // direct access to queue and count
  StackQueue<ReadyQueueLink> queue[NumPriority];
#else
  StackQueue<ReadyQueueLink> queue;
#endif
  size_t count;
public:
  StackResumeQueue() : count(0) {}
  bool empty() const { return count == 0; }
  size_t size() const { return count; }
  void push(StackContext& s) {
    Runtime::debugS("Stack ", FmtHex(&s), " queueing on ", FmtHex(this));
#if TESTING_POLLER_DIRECT
    GENASSERT1(s.getPriority() < NumPriority, s.getPriority());
    queue[s.getPriority()].push(s);
#else
    queue.push(s);
#endif
    count += 1;
  }
  StackContext* pop() {
    count -= 1;
#if TESTING_POLLER_DIRECT
    for (size_t p = 0; p < NumPriority; p += 1) {
      if (!queue[p].empty()) return queue[p].pop();
    }
    GENABORT1(FmtHex(this));
#else
    return queue.pop();
#endif
  }
};

class ReadyQueue {
  SystemLock readyLock;
#if TESTING_CONCURRENT_READYQUEUE
  StackMPSC<ReadyQueueLink> queue[NumPriority];
#else
  StackQueue<ReadyQueueLink> queue[NumPriority];
#endif

  ReadyQueue(const ReadyQueue&) = delete;            // no copy
  ReadyQueue& operator=(const ReadyQueue&) = delete; // no assignment

  StackContext* dequeueInternal() {
#if TESTING_CONCURRENT_READYQUEUE
    for (size_t p = 0; p < NumPriority; p += 1) {
      StackContext* s = queue[p].pop();
      if (s) return s;
    }
#else
    for (size_t p = 0; p < NumPriority; p += 1) {
      if (!queue[p].empty()) return queue[p].pop();
    }
#endif
    return nullptr;
  }

public:
  ReadyQueue() = default;

  StackContext* dequeue() {
    ScopedLock<SystemLock> sl(readyLock);
    return dequeueInternal();
  }

  StackContext* tryDequeue() {
    if (!readyLock.tryAcquire()) return nullptr;
    StackContext* s = dequeueInternal();
    readyLock.release();
    return s;
  }

  void enqueue(StackContext& s) {
    GENASSERT1(s.getPriority() < NumPriority, s.getPriority());
#if !TESTING_CONCURRENT_READYQUEUE
    ScopedLock<SystemLock> sl(readyLock);
#endif
    queue[s.getPriority()].push(s);
  }

#if TESTING_POLLER_DIRECT
  void enqueue(StackResumeQueue& srq) {
    srq.count = 0;
#if !TESTING_CONCURRENT_READYQUEUE
    ScopedLock<SystemLock> sl(readyLock);
#endif
    for (size_t p = 0; p < NumPriority; p += 1) {
      queue[p].transferAllFrom(srq.queue[p]);
    }
  }
#endif
};

class BaseProcessor;
typedef IntrusiveList<BaseProcessor,0,2> ProcessorList;
typedef IntrusiveRing<BaseProcessor,1,2> ProcessorRing;

class BaseProcessor : public ProcessorRing::Link {
  inline StackContext* tryLocal();
  inline StackContext* tryStage();
  inline StackContext* trySteal();
  inline StackContext& scheduleInternal();

  ReadyQueue readyQueue;

  void idleLoopTerminate();

protected:
  size_t        stackCount;
  Cluster&      cluster;
  StackContext* idleStack;

  ProcessorStats* stats;

  void idleLoop();

  void yieldDirect(StackContext& sc) {
    StackContext::idleYieldTo(sc, _friend<BaseProcessor>());
  }

#if TESTING_POLLER_DIRECT
  void enqueueDirect(StackResumeQueue& srq) {
    Runtime::debugS("Queue ", FmtHex(&srq), " bulk-queueing ", srq.size(), " on ", FmtHex(this));
    stats->bulk.add(srq.size());
    readyQueue.enqueue(srq);
  }
#endif

  void enqueueDirect(StackContext& s) {
    Runtime::debugS("Stack ", FmtHex(&s), " queueing on ", FmtHex(this));
    stats->enq.count();
    readyQueue.enqueue(s);
  }

public:
  BaseProcessor(Cluster& c, const char* n = "Processor") : stackCount(0), cluster(c), idleStack(nullptr) {
    stats = new ProcessorStats(this, n);
  }

  Cluster& getCluster() { return cluster; }

  void addStack(_friend<StackContext>) {
//    __atomic_add_fetch(&stackCount, 1, __ATOMIC_RELAXED);
  }
  void removeStack(_friend<StackContext>) {
//    __atomic_sub_fetch(&stackCount, 1, __ATOMIC_RELAXED);
  }

  StackContext* tryDequeue(_friend<Cluster>) {
    return readyQueue.tryDequeue();
  }

#if TESTING_POLLER_DIRECT
  void enqueueDirect(StackResumeQueue& srq, _friend<Scheduler>) {
    enqueueDirect(srq);
  }
#endif

  void enqueueDirect(StackContext& s, _friend<Cluster>) {
    enqueueDirect(s);
  }

  void enqueueDirect(StackContext& s, _friend<StackContext>) {
    enqueueDirect(s);
  }

  void enqueueResume(StackContext& s, _friend<StackContext>);

  StackContext& scheduleFull(_friend<StackContext>);
  StackContext* scheduleYield(_friend<StackContext>);
  StackContext* scheduleYield2(_friend<StackContext>);
};

#endif /* _BaseProcessor_h_ */
