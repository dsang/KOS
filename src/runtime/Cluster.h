/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Cluster_h_
#define _Cluster_h_ 1

#include "runtime/RuntimeTypes.h"

class Scheduler {
  volatile ssize_t stackCounter;
  SystemLock procLock;
  ProcessorList blockedProcs;
  BlockedStackList unblockStacks;

  SchedulerStats* stats;

  StackContext* block(BaseProcessor& proc) {
    procLock.acquire();
    if (unblockStacks.empty()) {
      blockedProcs.push_front(proc);
      procLock.release();
      StackResumeQueue srq;
#if TESTING_POLLER_DIRECT
      for (;;) {
        StackContext* nextStack = reinterpret_cast<SystemProcessor&>(proc).suspend(srq);
        if (srq.empty()) return nextStack;
        addReadyStacks(srq);
        proc.enqueueDirect(srq, _friend<Scheduler>());
        if (nextStack) return nextStack;
      }
#else
      return reinterpret_cast<SystemProcessor&>(proc).suspend(srq);
#endif
    } else {
      StackContext* nextStack = unblockStacks.pop_front();
      procLock.release();
      return nextStack;
    }
  }

  void unblockLocked(StackContext& sc) {
    if (blockedProcs.empty()) {
      unblockStacks.push_back(sc);
    } else {
      SystemProcessor* anyProc = reinterpret_cast<SystemProcessor*>(blockedProcs.pop_front());
      anyProc->resume(&sc);
    }
  }

  void unblock(StackContext& sc) {
    ScopedLock<SystemLock> sl(procLock);
    unblockLocked(sc);
  }

public:
  Scheduler() : stackCounter(0) { stats = new SchedulerStats(this); }

  bool tryGetReadyStack() {
    ssize_t c = stackCounter;
    return (c > 0) && __atomic_compare_exchange_n(&stackCounter, &c, c-1, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
  }

  StackContext* getReadyStack(BaseProcessor& proc) {
    stats->tasks.count();
    ssize_t blockedCount = - __atomic_sub_fetch(&stackCounter, 1, __ATOMIC_RELAXED);
    if (blockedCount > 0) {
      stats->blocks.count(blockedCount);
      return block(proc);
    }
    return nullptr;
  }

  bool addReadyStack(StackContext& sc) {
    if (__atomic_add_fetch(&stackCounter, 1, __ATOMIC_RELAXED) > 0) return false;
    unblock(sc);
    return true;
  }

#if TESTING_POLLER_DIRECT || TESTING_POLLER_BULK_ENQUEUE
  void addReadyStacks(StackResumeQueue& srq) {
    ssize_t oldCounter = __atomic_fetch_add(&stackCounter, (ssize_t)srq.size(), __ATOMIC_RELAXED);
    if (oldCounter < 0) {
      size_t wakeCount = std::min((ssize_t)srq.size(), -oldCounter);
      ScopedLock<SystemLock> sl(procLock);
      for (size_t i = 0; i < wakeCount; i += 1) unblockLocked(*srq.pop());
    }
  }
#endif
};

class Cluster : public Scheduler {
protected:
  ClusterLock    ringLock;
  size_t         ringCount;
  BaseProcessor* placeProc;
  BaseProcessor  stagingProc;

public:
  Cluster() : ringCount(0), placeProc(nullptr), stagingProc(*this, "Staging") {}
  ~Cluster() {
    ScopedLock<ClusterLock> sl(ringLock);
    GENASSERT1(!ringCount, ringCount);
  }

  void addProcessor(BaseProcessor& proc) {
    ScopedLock<ClusterLock> sl(ringLock);
    if (placeProc == nullptr) {
      ProcessorRing::close(proc);
      placeProc = &proc;
    } else {
      ProcessorRing::insert_after(*placeProc, proc);
    }
    ringCount += 1;
  }

  void removeProcessor(BaseProcessor& proc) {
    ScopedLock<ClusterLock> sl(ringLock);
    GENASSERT(placeProc);
    // move placeProc, if necessary
    if (placeProc == &proc) placeProc = ProcessorRing::next(*placeProc);
    // ring empty?
    if (placeProc == &proc) placeProc = nullptr;
    ProcessorRing::remove(proc);
    ringCount -= 1;
  }

  BaseProcessor& placement(_friend<StackContext>, bool sg = false) {
#if TESTING_PLACEMENT_RR
    if (sg) return stagingProc;
    GENASSERT(placeProc);
    ScopedLock<ClusterLock> sl(ringLock);
    placeProc = ProcessorRing::next(*placeProc); // ring insert/remove is traversal-safe
    return *placeProc;
#else
    return stagingProc;
#endif
  }

#if TESTING_POLLER_BULK_ENQUEUE
  void enqueueBulk(StackResumeQueue& srq) {
    addReadyStacks(srq);
    while (!srq.empty()) {
      StackContext* s = srq.pop();
      s->getProcessor().enqueueDirect(*s, _friend<Cluster>());
    }
  }
#endif

  StackContext* stage()  {
    return stagingProc.tryDequeue(_friend<Cluster>());
  }

  StackContext* steal(BaseProcessor& proc) {
    BaseProcessor* p = ProcessorRing::next(proc);
    for (;;) {
      StackContext* s = p->tryDequeue(_friend<Cluster>());
      if (s) return s;
      p = ProcessorRing::next(*p); // ring insert/remove is traversal-safe
      if (p == &proc) return nullptr;
    }
  }
};

#endif /* _Cluster_h_ */
