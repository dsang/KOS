/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Cluster.h"
#include "runtime/RuntimeImpl.h"

inline StackContext* BaseProcessor::tryLocal() {
  StackContext* s = readyQueue.dequeue();
  if (s) {
    Runtime::debugS("tryLocal: ", FmtHex(this), ' ', FmtHex(s));
    stats->deq.count();
  }
  return s;
}

inline StackContext* BaseProcessor::tryStage() {
  StackContext* s = cluster.stage();
  if (s) {
    Runtime::debugS("tryStage: ", FmtHex(this), ' ', FmtHex(s));
    if (s->getAffinity()) {
      stats->borrow.count();
    } else {
      stats->stage.count();
      s->changeProcessor(*this, _friend<BaseProcessor>());
    }
  }
  return s;
}

inline StackContext* BaseProcessor::trySteal() {
  StackContext* s = cluster.steal(*this);
  if (s) {
    Runtime::debugS("trySteal: ", FmtHex(this), ' ', FmtHex(s));
    stats->steal.count();
  }
  return s;
}

inline StackContext& BaseProcessor::scheduleInternal() {
  for (;;) {
    StackContext* nextStack;
    if ((nextStack = tryLocal())) return *nextStack;
    if ((nextStack = tryStage())) return *nextStack;
    if ((nextStack = trySteal())) return *nextStack;
  }
}

void BaseProcessor::idleLoop() {
  for (;;) {
    StackContext* nextStack;
#if TESTING_POLLER_DIRECT
    StackResumeQueue srq;
    nextStack = reinterpret_cast<SystemProcessor*>(this)->trypoll(srq);
    GENASSERT(!nextStack);
    if (!srq.empty()) {
      nextStack = srq.pop();
      cluster.addReadyStacks(srq);
      enqueueDirect(srq);
      yieldDirect(*nextStack);
  continue;
    }
#endif
    nextStack = cluster.getReadyStack(*this);
    if (nextStack) {
      stats->handover.count();
      yieldDirect(*nextStack);
    } else {
      yieldDirect(scheduleInternal());
    }
  }
}

void BaseProcessor::enqueueResume(StackContext& s, _friend<StackContext> fsc) {
  if (!cluster.addReadyStack(s)) enqueueDirect(s, fsc);
}

StackContext& BaseProcessor::scheduleFull(_friend<StackContext>) {
  if (cluster.tryGetReadyStack()) return scheduleInternal();
  return *idleStack;
}

StackContext* BaseProcessor::scheduleYield(_friend<StackContext>) {
  return tryLocal();
}

StackContext* BaseProcessor::scheduleYield2(_friend<StackContext>) {
  StackContext* nextStack = tryLocal();
  if (nextStack) return nextStack;
  return trySteal();
}
