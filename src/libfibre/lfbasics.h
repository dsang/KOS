/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _lfbasics_h_
#define _lfbasics_h_ 1

#include "generic/basics.h"

#include <atomic>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/syscall.h>
#if __FreeBSD__
#include <sys/thr.h>
#endif

// **** bootstrap object needs to come first

static class _Bootstrapper {
  static std::atomic<int> counter;
public:
  _Bootstrapper();
  ~_Bootstrapper();
} _lfBootstrap;

// errno is TLS, so must no be inlined
// see, for example, http://www.crystalclearsoftware.com/soc/coroutine/coroutine/coroutine_thread.html
extern int lfErrno() __no_inline;
extern int& lfErrnoSet() __no_inline;

// internal abort helper with backtrace
extern void _lfAbort() __noreturn;

// **** checks and balances, using gcc/clang's 'expression statement' extension

#if TESTING_ENABLE_ASSERTIONS
static void _SYSCALLabort() __no_inline;
static void _SYSCALLabort() { _lfAbort(); }
extern void _SYSCALLabortLock();
extern void _SYSCALLabortUnlock();
#include "syscall_macro.h"
#else
#define SYSCALL_CMP(call,cmp,expected,errcode) call
#endif

#define SYSCALL(call)            SYSCALL_CMP(call,==,0,0)
#define SYSCALL_EQ(call,val)     SYSCALL_CMP(call,==,val,0)
#define SYSCALL_GE(call,val)     SYSCALL_CMP(call,>=,val,0)
#define TRY_SYSCALL(call,code)   SYSCALL_CMP(call,==,0,code)
#define SYSCALLIO(call)          SYSCALL_CMP(call,>=,0,0)
#define TRY_SYSCALLIO(call,code) SYSCALL_CMP(call,>=,0,code)
#define uabort(msg) { std::cerr << msg << std::endl; lfAbort(); }

// **** locking

template<size_t SpinStart, size_t SpinEnd, size_t SpinCount>
class OsLock {
  pthread_mutex_t mutex;
  friend class OsCondition;
public:
  OsLock() : mutex(PTHREAD_MUTEX_INITIALIZER) {}
  ~OsLock() {
    SYSCALL(pthread_mutex_destroy(&mutex));
  }
  bool tryAcquire() {
    return pthread_mutex_trylock(&mutex) == 0;
  }
  void acquire() {
    for (size_t cnt = 0; cnt < SpinCount; cnt += 1) {
      for (size_t spin = SpinStart; spin <= SpinEnd; spin += spin) {
        if fastpath(tryAcquire()) return;
        for (size_t i = 0; i < spin; i += 1) Pause();
      }
    }
    SYSCALL(pthread_mutex_lock(&mutex));
  }
  bool acquire(const Time& timeout) {
    return TRY_SYSCALL(pthread_mutex_timedlock(&mutex, &timeout), ETIMEDOUT) == 0;
  }
  void release() {
    SYSCALL(pthread_mutex_unlock(&mutex));
  }
  bool test() {
    if (!tryAcquire()) return true;
    release();
    return false;
  }
};

class OsCondition {
  pthread_cond_t cond;
public:
  OsCondition() : cond(PTHREAD_COND_INITIALIZER) {}
  ~OsCondition() {
    SYSCALL(pthread_cond_destroy(&cond));
  }
  template<typename Lock>
  void clear(Lock& lock) {
    SYSCALL(pthread_cond_broadcast(&cond));
    lock.release();
  }
  template<typename Lock>
  void wait(Lock& lock) {
    SYSCALL(pthread_cond_wait(&cond, &lock.mutex));
  }
  template<typename Lock>
  bool wait(Lock& lock, const Time& timeout) {
    return TRY_SYSCALL(pthread_cond_timedwait(&cond, &lock.mutex, &timeout), ETIMEDOUT) == 0;
  }
  void signal() {
    SYSCALL(pthread_cond_signal(&cond));
  }
};

template<size_t SpinStartRead, size_t SpinEndRead, size_t SpinCountRead, size_t SpinStartWrite, size_t SpinEndWrite, size_t SpinCountWrite>
class OsLockRW {
  pthread_rwlock_t rwlock;
public:
  OsLockRW() {
    SYSCALL(pthread_rwlock_init(&rwlock, nullptr));
  }
  ~OsLockRW() {
    SYSCALL(pthread_rwlock_destroy(&rwlock));
  }
  bool tryAcquireRead() {
    return pthread_rwlock_tryrdlock(&rwlock) == 0;
  }
  void acquireRead() {
    for (size_t cnt = 0; cnt < SpinCountRead; cnt += 1) {
      for (size_t spin = SpinStartRead; spin <= SpinEndRead; spin += spin) {
        if fastpath(tryAcquireRead()) return;
        for (size_t i = 0; i < spin; i += 1) Pause();
      }
    }
    SYSCALL(pthread_rwlock_rdlock(&rwlock));
  }
  bool acquireRead(const Time& timeout) {
    return TRY_SYSCALL(pthread_rwlock_timedrdlock(&rwlock, &timeout), ETIMEDOUT) == 0;
  }
  bool tryAcquireWrite() {
    return pthread_rwlock_trywrlock(&rwlock) == 0;
  }
  void acquireWrite() {
    for (size_t cnt = 0; cnt < SpinCountWrite; cnt += 1) {
      for (size_t spin = SpinStartWrite; spin <= SpinEndWrite; spin += spin) {
        if fastpath(tryAcquireWrite()) return;
        for (size_t i = 0; i < spin; i += 1) Pause();
      }
    }
    SYSCALL(pthread_rwlock_wrlock(&rwlock));
  }
  bool acquireWrite(const Time& timeout) {
    return TRY_SYSCALL(pthread_rwlock_timedwrlock(&rwlock, &timeout), ETIMEDOUT) == 0;
  }
  void release() {
    SYSCALL(pthread_rwlock_unlock(&rwlock));
  }
};

class OsSemaphore {
  sem_t sem;
public:
  OsSemaphore(size_t c = 0) {
    SYSCALL(sem_init(&sem, 0, c));
  }
  ~OsSemaphore() {
    SYSCALL(sem_destroy(&sem));
  }
  bool empty() {
    int val;
    SYSCALL(sem_getvalue(&sem, &val));
    return val >= 0;
  }
  bool open() {
    int val;
    SYSCALL(sem_getvalue(&sem, &val));
    return val > 0;
  }
  bool tryP() {
    for (;;) {
      int ret = sem_trywait(&sem);
      if (ret == 0) return true;
      else if (lfErrno() == EAGAIN) return false;
      else { GENASSERT1(lfErrno() == EINTR, lfErrno()); }
    }
  }
  bool P(bool wait = true) {
    if (!wait) return tryP();
    while (sem_wait(&sem) < 0) { GENASSERT1(lfErrno() == EINTR, lfErrno()); }
    return true;
  }
  bool P(const Time& timeout) {
    for (;;) {
      int ret = sem_timedwait(&sem, &timeout);
      if (ret == 0) return true;
      else if (lfErrno() == ETIMEDOUT) return false;
      else { GENASSERT1(lfErrno() == EINTR, lfErrno()); }
    }
  }
  void V() {
    SYSCALL(sem_post(&sem));
  }
};

//typedef OsLock<0,0,0> InternalLock;
typedef OsLock<4,1024,1> InternalLock;
//typedef BinaryLock<> InternalLock;

#if 0 /* unused */
template<bool Binary>
class FlexOsSemaphore {
  InternalLock lock;
  OsCondition cond;
  ssize_t counter;
  size_t signal;
public:
  FlexOsSemaphore(ssize_t c = 0) : counter(c), signal(0) {}
  bool empty() { return counter >= 0; }
  bool open() { return counter > 0; }
  bool P() {
    ScopedLock<InternalLock> sl(lock);
    counter -= 1;
    if (counter < 0) {
      while (!signal) cond.wait(lock);
      signal -= 1;
    }
    return true;
  }
  bool tryP() {
    ScopedLock<InternalLock> sl(lock);
    if (counter < 1) return false;
    counter -= 1;
    return true;
  }
  void V() {
    ScopedLock<InternalLock> sl(lock);
    counter += 1;
    if (counter < 1) {
      signal += 1;
      cond.signal();
    } else if (Binary) counter = 1;
  }
};
#endif

// **** debug output

extern InternalLock* _lfDebugOutputLock;

inline void dprint() {}

template<typename T, typename... Args>
inline void dprint(T x, const Args&... a) {
  std::cerr << x;
  dprint(a...);
}

template<typename... Args>
inline void dprintl(const Args&... a) {
  ScopedLock<InternalLock> al(*_lfDebugOutputLock);
#if __FreeBSD__
  long tid;
  thr_self(&tid);
  dprint(tid, ' ', a..., '\n');
#else // __linux__ below
  dprint(syscall(__NR_gettid), ' ', a..., '\n');
#endif
}

// **** system processor (here pthread) context

struct Runtime;
class BasePoller;
class EventScope;
class FibreCluster;
class OsProcessor;
class Poller;
class StackContext;

// it seems noinline is needed for TLS and then volatile is free anyway...
// http://stackoverflow.com/questions/25673787/making-thread-local-variables-fully-volatile
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66631
class Context {
protected: // definitions and initialization are in OsProcessor.cc
  static thread_local StackContext* volatile currStack;
  static thread_local OsProcessor*  volatile currProc;
  static thread_local FibreCluster* volatile currCluster;
  static thread_local EventScope*   volatile currScope;
  static thread_local BasePoller*   volatile currPoller;
  static thread_local Poller*       volatile clusterPoller;
public:
  static void setCurrStack(StackContext& s, _friend<Runtime>) __no_inline;
  static StackContext* CurrStack()      __no_inline;
  static OsProcessor*  CurrProcessor()  __no_inline;
  static FibreCluster* CurrCluster()    __no_inline;
  static EventScope*   CurrEventScope() __no_inline;
  static BasePoller*   CurrPoller()     __no_inline;
  static Poller*       ClusterPoller()  __no_inline;
};

static inline StackContext* CurrStack() {
  StackContext* s = Context::CurrStack();
  GENASSERT(s);
  return s;
}

static inline OsProcessor& CurrProcessor() {
  OsProcessor* p = Context::CurrProcessor();
  GENASSERT(p);
  return *p;
}

static inline FibreCluster& CurrCluster() {
  FibreCluster* c = Context::CurrCluster();
  GENASSERT(c);
  return *c;
}

static inline EventScope& CurrEventScope() {
  EventScope* e = Context::CurrEventScope();
  GENASSERT(e);
  return *e;
}

static inline BasePoller& CurrPoller() {
  BasePoller* p = Context::CurrPoller();
  GENASSERT(p);
  return *p;
}


static inline Poller& ClusterPoller() {
  Poller* p = Context::ClusterPoller();
  GENASSERT(p);
  return *p;
}

// **** global constants

#ifdef SPLIT_STACK
static const size_t defaultStackSize =  2 * pagesize<1>();
#else
static const size_t defaultStackSize = 16 * pagesize<1>();
#endif
static const size_t stackProtection = pagesize<1>();

#endif /* _lfbasics_h_ */
