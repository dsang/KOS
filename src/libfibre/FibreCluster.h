/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _FibreCluster_h_
#define _FibreCluster_h_ 1

#include "runtime/Cluster.h"
#include "libfibre/Poller.h"

class FibreCluster : public Cluster {
  EventScope&  scope;
  Poller       poller;

  enum State { Run, Pause } state;

  FibreSemaphore  maintStartSem;
  FibreSemaphore  confirmSem;
  FibreSemaphore  continueSem;
  SystemSemaphore pauseSem;
  OsProcessor*    maintenanceProc;

  FibreCluster(EventScope& es, _friend<FibreCluster>) : scope(es), poller(es, *this), state(Run), pauseSem(1), maintenanceProc(nullptr) {}
public:
  FibreCluster(EventScope& es) : FibreCluster(es, _friend<FibreCluster>()) { poller.start(); }
  FibreCluster() : FibreCluster(CurrEventScope()) {}

  FibreCluster(EventScope& es, _friend<EventScope>) : FibreCluster(es, _friend<FibreCluster>()) {}
  void startPoller(_friend<EventScope>) { poller.start(); }

  EventScope& getEventScope() { return scope; }
  Poller& getPoller() { return poller; }

  void pause() {
    ringLock.acquire();
    state = Pause;
    maintenanceProc = &CurrProcessor();
    pauseSem.P();
    for (size_t p = 0; p < ringCount; p += 1) maintStartSem.V();
    for (size_t p = 0; p < ringCount; p += 1) confirmSem.P();
  }

  void resume() {
    pauseSem.V();
    ringLock.release();
  }

  void wakeAll() {
    ringLock.acquire();
    state = Run;
    maintenanceProc = nullptr;
    for (size_t p = 0; p < ringCount; p += 1) maintStartSem.V();
    for (size_t p = 0; p < ringCount; p += 1) confirmSem.P();
    for (size_t p = 0; p < ringCount; p += 1) continueSem.V();
    ringLock.release();
  }

  static void maintenance(FibreCluster* fc, OsProcessor* proc) {
    for (;;) {
      fc->maintStartSem.P();
      switch (fc->state) {
      case Pause:
        fc->confirmSem.V();
        if (fc->maintenanceProc != &CurrProcessor()) {
          fc->pauseSem.P();
          fc->pauseSem.V();
        }
        break;
      case Run:
        fc->confirmSem.V();
        fc->continueSem.P();
        break;
      default:
        GENABORT1(fc->state);
      }
    }
  }
};

#endif /* _FibreCluster_h_ */
