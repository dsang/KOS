// **** general options - testing

#define TESTING_ENABLE_ASSERTIONS     1
#define TESTING_ENABLE_STATISTICS     1
#define TESTING_ENABLE_DEBUGGING      1
#define TESTING_ALWAYS_MIGRATE        1 // KOS only
#define TESTING_DISABLE_HEAP_CACHE    1 // KOS only

// **** general options - safer execution

//#define TESTING_DISABLE_ALLOC_LAZY    1 // KOS only
//#define TESTING_DISABLE_DEEP_IDLE     1 // KOS only
//#define TESTING_DISABLE_PREEMPTION    1 // KOS only
//#define TESTING_REPORT_INTERRUPTS     1 // KOS only

// **** general options - alternative design

#define TESTING_CONCURRENT_READYQUEUE 1 // vs. traditional locking
#define TESTING_NEMESIS_READYQUEUE    1 // vs. stub-based MPSC
#define TESTING_IDLE_SPIN         65536 // spin before idle/halt
//#define TESTING_MUTEX_FIFO            1 // use fifo/baton mutex
//#define TESTING_MUTEX_BARGING         1 // use blocking/barging mutex
//#define TESTING_MUTEX_SPIN            1 // spin before block in non-fifo mutex
#define TESTING_PLACEMENT_RR          1 // RR placement, instead of load-based staging

// **** libfibre options - event handling

//#define TESTING_POLLER_DIRECT         1 // poll events directly from each worker
#define TESTING_POLLER_FIBRE          1 // vs. per-cluster poller pthread
#define TESTING_POLLER_FIBRE_SPIN 65536 // spin loop of NB polls
//#define TESTING_POLLER_BULK_ENQUEUE   1 // partial bulk enqueue from poller
#define TESTING_LAZY_FD_REGISTRATION  1 // vs. eager registration after fd creation
//#define TESTING_IOYIELD_CONDITIONAL   1 // yield-before-read only when FD not registered for poll
//#define TESTING_TRY_IO_BEFORE_YIELD   1 // make one non-blocking I/O attempt before yield

// **** KOS console/serial output configuration

//#define TESTING_DEBUG_STDOUT          1
#define TESTING_STDOUT_DEBUG          1
#define TESTING_STDERR_DEBUG          1

// **** KOS-specific tests

//#define TESTING_KEYCODE_LOOP          1
//#define TESTING_LOCK_TEST             1
#define TESTING_TCP_TEST              1
#define TESTING_MEMORY_HOG            1
#define TESTING_PING_LOOP             1
//#define TESTING_TIMER_TEST            1
