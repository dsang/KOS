include $(SRCDIR)/../config
CFGDIR:=$(SRCDIR)/../cfg

GCC=$(GCCDIR)/bin/$(TARGET)-gcc
GPP=$(GCCDIR)/bin/$(TARGET)-g++
ifeq ($(shell uname -s),FreeBSD)
CC=clang
else
CC=$(GCC)
endif
ifeq ($(CC),clang)
CXX=clang++
else
CXX=$(GPP)
endif

AS=$(GCCDIR)/bin/$(TARGET)-as
AR=$(GCCDIR)/bin/$(TARGET)-ar
LD=$(GCCDIR)/bin/$(TARGET)-ld
RANLIB=$(GCCDIR)/bin/$(TARGET)-ranlib
STRIP=$(GCCDIR)/bin/$(TARGET)-strip
GDB=$(GDBDIR)/bin/$(TARGET)-gdb -b 38400 -x $(CFGDIR)/gdbinit1

IMAGE=/tmp/KOS.img
ISO=/tmp/KOS.iso
STAGE=stage

ifeq ($(DISP),vnc)
QEMU_DISP=-display vnc=:0 # vncviewer localhost
else
QEMU_DISP=-display sdl
endif

#QEMU_LOG=-d int,cpu_reset

QEMU=$(QEMUDIR)/bin/qemu-system-x86_64 -nodefconfig $(QEMU_DISP) $(QEMU_LOG)\
	-m 1024 -smp cores=2,threads=1,sockets=2 -rtc base=utc\
  -debugcon file:/tmp/KOS.dbg

QEMU_UNET=-device e1000,netdev=hn0 -netdev user,id=hn0,restrict=off,tftp=$(TFTPDIR),bootfile=pxelinux.0
QEMU_RNET=-device e1000,netdev=hn0 -netdev bridge,id=hn0,br=br0

QEMU_IMG=-boot order=d -cdrom $(ISO)
#QEMU_IMG=-boot order=c -hda $(IMAGE)
QEMU_PXE=-boot order=n

QEMU_SER=-serial file:/tmp/KOS.serial
QEMU_GDB=-serial tcp::2345,server $(QEMU_SER)

TMPFILES=/tmp/KOS.dbg /tmp/KOS.serial

LANGFLAGS=-std=c++11 -fno-rtti -fno-exceptions

OPTIM?=1
ifeq ($(OPTIM),1)
OPTFLAGS=-O3 -fno-omit-frame-pointer
endif

DBGFLAGS=-ggdb

COMPFLAGS=-fno-common -Wall -Wextra\
  -Wno-unused-function -Wno-unused-parameter

KERNBASE=0xFFFFFFFF80000000

KERNFLAGS=-D__KOS__ -DKERNEL -DKERNBASE=$(KERNBASE)

CFGFLAGS=-I$(SRCDIR) -I$(SRCDIR)/include # -include $(SRCDIR)/testoptions.h

MACHFLAGS=-ffreestanding -mcmodel=kernel -m64\
	-mno-red-zone -mno-mmx -mno-avx -mno-avx2 -mno-3dnow\
	-mno-sse -mno-sse2 -mno-sse3 -mno-sse4 -mno-sse4a -mno-sse4.1 -mno-sse4.2
# -mpopcnt

CLANGFLAGS=-nostdinc++ -target $(TARGET)\
	 -Wno-unused-private-field -Wno-attributes\
	-I$(GCCDIR)/lib/gcc/$(TARGET)/$(GCCVER)/include\
	-I$(GCCDIR)/$(TARGET)/include/c++/$(GCCVER)/$(TARGET)\
	-I$(GCCDIR)/$(TARGET)/include/c++/$(GCCVER)\
	-I$(GCCDIR)/$(TARGET)/include

ifeq ($(CC),clang)
COMPFLAGS+=$(CLANGFLAGS)
else
COMPFLAGS+=-Wstack-usage=2048
endif

TSTFLAGS=$(LANGFLAGS) $(OPTFLAGS) $(DBGFLAGS) $(COMPFLAGS) $(KERNFLAGS) $(CFGFLAGS)
PREFLAGS=$(LANGFLAGS) $(OPTFLAGS)             $(COMPFLAGS) $(KERNFLAGS) $(CFGFLAGS) $(MACHFLAGS)
CXXFLAGS=$(LANGFLAGS) $(OPTFLAGS) $(DBGFLAGS) $(COMPFLAGS) $(KERNFLAGS) $(CFGFLAGS) $(MACHFLAGS)
CFLAGS=               $(OPTFLAGS) $(DBGFLAGS) $(COMPFLAGS) $(KERNFLAGS)             $(MACHFLAGS)

ASFLAGS=-g --64 --divide -I$(SRCDIR) -I$(SRCDIR)/include\
	--defsym KERNBASE=$(KERNBASE) --defsym __x86_64__=1

LDFLAGS=-nodefaultlibs -static
LDDEF=--defsym=KERNBASE=$(KERNBASE)
LIBS=-L$(GCCDIR)/$(TARGET)/lib -L$(GCCDIR)/lib/gcc/$(TARGET)/$(GCCVER)\
  -lstdc++ -lc -lgcc

CXXFLAGS+=-Iextern/lwip\
	-Iextern/lwip/lwip/src/include\
	-Iextern/lwip/lwip/src/include/ipv4
