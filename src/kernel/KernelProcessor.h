/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _KernelProcessor_h_
#define _KernelProcessor_h_ 1

#include "extern/mtrand/mtrand.h"
#include "generic/IntrusiveContainers.h"
#include "runtime/BaseProcessor.h"
#include "kernel/FrameManager.h"
#include "machine/HardwareProcessor.h"

class Cluster;
class Machine;

struct AddressSpaceMarker : public IntrusiveList<AddressSpaceMarker>::Link {
  volatile sword enterEpoch;
};

static inline KernelProcessor& CurrProcessor();
static inline Cluster& CurrCluster();

template<typename T>
class _FakeQueue {
  T* waiter;
public:
  _FakeQueue() : waiter(nullptr) {}
  bool empty() { return waiter == nullptr; }
  void push_back(T& elem) { waiter = &elem; }
  T* pop_front() {
    T* ret = waiter;
    waiter = nullptr;
    return ret;
  }
};

template<bool Binary, typename Queue = ProcessorList>
class KernelSemaphore {
  KernelLock lock;
  Queue queue;
  size_t counter;
  size_t signal;
public:
  KernelSemaphore(size_t c = 0) : counter(c), signal(0) {}
  inline bool P(KernelProcessor& proc = CurrProcessor());
  bool tryP() {
    ScopedLock<KernelLock> sl(lock);
    if (counter < 1) return false;
    counter -= 1;
    return true;
  }
  inline void V();
};

typedef KernelSemaphore<true,_FakeQueue<KernelProcessor>> CoreSemaphore;

class KernelProcessor : public BaseProcessor, public HardwareProcessor {
  friend class KernelAddressSpace; // kernASM
  friend class AddressSpace;       // userASM
  AddressSpaceMarker kernASM;
  AddressSpaceMarker userASM;
  MTRand_int32 rng;
  bool maskIRQs;

  CoreSemaphore haltNotify;
  StackContext* handoverStack;

  static void idleLoopSetup(KernelProcessor*) __noreturn;

public:
  KernelProcessor(Cluster& c = CurrCluster()) : BaseProcessor(c), HardwareProcessor(this),
    maskIRQs(false), handoverStack(nullptr)  {}
  void start(funcvoid0_t func, bool allowIRQs) __noreturn;

  void seedRNG(mword s) { rng.seed(s); }
  mword random() { return rng(); }

  void halt(KernelLock& lock, size_t& signal) {
    DBG::outl(DBG::Idle, "entering ", (maskIRQs ? "deep" : "light"), " idle");
    if (maskIRQs) MappedAPIC()->maskTimer();
    while (!signal) {
      lock.releaseHalt(); // might be woken up by other interrupts
      lock.acquireHalt(); // -> need to check 'signal' again
    }
    if (maskIRQs) MappedAPIC()->unmaskTimer();
  }

#if TESTING_POLLER_DIRECT
  StackContext* trypoll(StackResumeQueue&) { return nullptr; }
#endif

  StackContext* suspend(StackResumeQueue& srq) {
#if TESTING_IDLE_SPIN
    static const size_t SpinMax = TESTING_IDLE_SPIN;
    for (size_t i = 0; i < SpinMax; i += 1) {
      if fastpath(haltNotify.tryP()) return handoverStack;
      Pause();
    }
#endif
    stats->idle.count();
    haltNotify.P(*this);
    return handoverStack;
  }
  void resume(StackContext* sc = nullptr) {
    handoverStack = sc;
    stats->wake.count();
    haltNotify.V();
  }
};

static inline KernelProcessor& CurrProcessor() {
  KernelProcessor* p = LocalProcessor::self();
  GENASSERT(p);
  return *p;
}

static inline Cluster& CurrCluster() {
  return CurrProcessor().getCluster();
}

template<bool Binary, typename Queue>
inline bool KernelSemaphore<Binary,Queue>::P(KernelProcessor& proc) {
  ScopedLock<KernelLock> sl(lock);
  if (counter < 1) {
    queue.push_back(proc);
    for (;;) {
      if (signal) {
        signal -= 1;
        return true;
      }
      lock.release();
      bool zero = CurrFM().zeroMemory<smallpl>();
      lock.acquire();
      if (!zero) break;
    }
    proc.halt(lock, signal);
    signal -= 1;
  } else {
    counter -= 1;
  }
  return true;
}

template<bool Binary, typename Queue>
inline void KernelSemaphore<Binary,Queue>::V() {
  lock.acquire();
  if (queue.empty()) {
    if (Binary) counter = 1;
    else counter += 1;
    lock.release();
  } else {
    KernelProcessor* proc = reinterpret_cast<KernelProcessor*>(queue.pop_front());
    signal += 1;
    lock.release();
    proc->sendWakeIPI();
    DBG::outl(DBG::Idle, "sent WakeIPI to ", proc->getIndex());
  }
}

#endif /* _KernelProcessor_h_ */
