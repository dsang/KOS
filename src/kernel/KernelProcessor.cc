/******************************************************************************
    Copyright (C) Martin Karsten 2015-2019

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Cluster.h"
#include "kernel/AddressSpace.h"
#include "kernel/KernelProcessor.h"
#include "kernel/Thread.h"

void KernelProcessor::idleLoopSetup(KernelProcessor* kp) {
  kp->idleLoop();
  unreachable();
}

void KernelProcessor::start(funcvoid0_t func, bool allowIRQs) {
#if TESTING_DISABLE_DEEP_IDLE
  this->maskIRQs = false;
#else
  this->maskIRQs = !allowIRQs;
#endif
  cluster.addProcessor(*this);
  kernelAS.initInvalidation(kernASM);
  defaultAS.initInvalidation(userASM);
  idleStack = Thread::create(*this, idleStackSize);
  idleStack->setAffinity(true);
  idleStack->setup((ptr_t)idleLoopSetup, (ptr_t)this);
  Thread* startThread = Thread::create(*this, defaultStackSize);
  HardwareProcessor::setupCurrStack(startThread);
  startThread->direct((ptr_t)func, _friend<KernelProcessor>());
}
